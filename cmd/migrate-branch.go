package cmd

import (
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// branchCmd represents the branch command
var branchCmd = &cobra.Command{
	Use:   "gitlab-buddy migrate branch <FROM_BRANCH> <TO_BRANCH> {[-h | --host] <HOST_NAME>} {[-r | --repo] <REPO_ID> | [-g | --group] <GROUP_ID>} [-a | --archive-old-branch] [-k | --keep-old-branch] [[-d | --set-default] | [-p | --set-protected-default]] [-o | --omit-merge-requests]",
	Short: "Migrates a branch to a new branch. Can be used on a single repo or all repos in a group",
	Long: `Migrates a branch to a new branch. Supports migration of an individual repo or all repos in a gorup.

Example 1:
Migrate the [master] branch to [main] for an individual repo, archive the [master] branch, then set [main] to the default branch and protect it.

> gitlabbuddy migrate branch master main --repo="valid-repo-id" --host=myhost --archive-old-branch --set-protected-default

Example 2:
Same as 'Example 1' but migrate all repos in a group from [master] to [main].

> gitlabbuddy migrate branch master main --group="valid-group-id" --host=myhost --archive-old-branch --set-protected-deafult
`,
	Run: func(cmd *cobra.Command, args []string) {
		// For some reason cobra won't print the "Use" text defined about by default.
		// I worked in a previous cobra verion. But for now, manually print Help() when no args are passed
		if len(args) == 0 {
			cmd.Help()
			os.Exit(0)
		}
	},
}

func init() {
	migrateCmd.AddCommand(branchCmd)

	branchCmd.Flags().StringP("host", "h", "", "Name of a host configured in the 'gitlab-buddy.yml' config")
	branchCmd.Flags().StringP("repo", "r", "", "The Gitlab numberic ID or URL-encoded path of the repo/project. (e.g., heb/sub/group/project) See Gitlab's 'Namespaced path encoding' docs for more detail")
	branchCmd.Flags().BoolP("set-default", "d", false, "Set the new branch as the default branch for the repo")
	branchCmd.Flags().BoolP("set-protected-default", "p", true, "Set the new branch as default and protect it. This overrides '--set-default' flag.")
	branchCmd.Flags().BoolP("archive-old-branch", "a", true, "Archive the old branch under the protected 'archive' tag and delete the old branch. The 'archive' tag is created/protected if not already in repo.")
	branchCmd.Flags().BoolP("keep-old-branch", "k", false, "Prevents old branch from being removed, as the old branch is removed by default")
	branchCmd.Flags().BoolP("omit-merge-requests", "o", false, "Prevents open merge requests from being migrated - targeting the new branch.")

	viper.BindPFlags(branchCmd.Flags())
}
