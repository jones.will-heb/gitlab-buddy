/*
Copyright © 2021 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"
	"os"

	"github.com/spf13/cobra"
	glb "gitlab.com/jones.will-heb/gitlab-buddy/internal"
	"gitlab.com/jones.will-heb/gitlab-buddy/internal/config"
	"go.uber.org/zap"
)

var cfgFilePath string

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "gitlab-buddy [-h | --help] [--config /path/file.yml]",
	Short: "Management tools for Gitlab",
	Long:  `Management tools for Gitlab. Currently, only a limited set of features is supported.`,
	Run: func(cmd *cobra.Command, args []string) {
		// For some reason cobra won't print the "Use" text defined about by default.
		// I worked in a previous cobra verion. But for now, manually print Help() when no args are passed
		if len(args) < 1 {
			cmd.Help()
			os.Exit(0)
		}
	},
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	cobra.CheckErr(rootCmd.Execute())
}

func init() {
	cobra.OnInitialize(initConfig)

	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.

	rootCmd.PersistentFlags().StringVar(&cfgFilePath, "config", "", "config file (default is $HOME/.gitlab-buddy.yaml)")

	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	rootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	
	// Check if config exists
	if cfgFilePath != "" {
		cf, err := config.NewConfigFile(cfgFilePath)
		if err != nil {
			return
		}

		err = config.LoadFile(cf)
		if err != nil {
			return
		}
	} else {
		// TODO: Move this into main.go, so that CLI isn't the only easy entry point
		// Attempt to load config file and/or defaults
		// Don't panic if error to allow all defaults to get through
		err := config.Load()
		if err != nil {
			fmt.Errorf("Error loading config: %w", err)
		}

		// Initialize global logger
		glb.InitLogger()
		zap.S().Debug("replaced zap's global loggers")
	}
}
