package cmd

import (
	// "fmt"

	"os"

	"github.com/spf13/cobra"
)

// migrateCmd represents the migrate command
var migrateCmd = &cobra.Command{
	Use:   "migrate",
	Short: "Migrate content of Gitlab Projects or Groups. NOTE: Currently only branch migration is supported",
	Long:  `Migrate content of Gitlab Projects or Groups. NOTE: Currently only branch migration is supported`,
	Run: func(cmd *cobra.Command, args []string) {
		// For some reason cobra won't print the "Use" text defined about by default.
		// I worked in a previous cobra verion. But for now, manually print Help() when no args are passed
		if len(args) < 1 {
			cmd.Help()
			os.Exit(0)
		}
	},

}

func init() {
	rootCmd.AddCommand(migrateCmd)
}
